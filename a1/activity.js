
	
a. What directive is used by Node.js in loading the modules it needs?
	Answers:
	require

b. what Node.js contains a method for server creation.
	Answers:
	http

c. what is the method of the http object responsible for creating a server using Node.js
	Answers:
	createServer
	
d. What method of the response object allows us to set status codes and content types?
	Answers:
	WriteHead

e. Where will console.log() output its contents when run in Node.js?
	Answers:
	terminal

f. What property of the request object contains the address endpoint.
	Answers:
	url

